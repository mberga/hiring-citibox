<?php

namespace App\Domain\BurrikingProduct\Service\Pricing;


use App\Domain\BurrikingProduct\Entity\Burger;
use App\Domain\BurrikingProduct\Entity\Ingredient;
use App\Domain\BurrikingProduct\ValueObject\Amount;

class BurgerCalculator implements PricingCalculator
{
    /**
     * @param Burger $object
     * @return Amount
     */
    public function calculate($object)
    {
        $price = Burger::BASE_PRICE;

        $price += $object->getBurgerSize()->getPrice()->getAmount();

        /** @var Ingredient $ingredient */
        foreach ($object->getIngredients() as $ingredient) {
            $price += $ingredient->getPrice()->getAmount();
        }

        return Amount::create($price);
    }

}
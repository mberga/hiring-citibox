<?php

namespace App\Domain\BurrikingProduct\Service\Pricing;


interface PricingCalculator
{
    public function calculate($object);
}
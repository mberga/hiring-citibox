<?php

namespace App\Domain\BurrikingProduct\Service\Factory;


use App\Domain\BurrikingProduct\Entity\Order;
use App\Domain\BurrikingProduct\Entity\User;
use App\Domain\BurrikingProduct\Repository\ComplementRepository;
use App\Domain\BurrikingProduct\Repository\UserRepository;

class OrderFactory implements BurrikingFactory
{
    protected $burgerFactory;
    protected $userRepository;
    protected $complementRepository;

    public function __construct(
        BurrikingFactory $burgerFactory,
        UserRepository $userRepository,
        ComplementRepository $complementRepository
    ) {
        $this->burgerFactory = $burgerFactory;
        $this->userRepository = $userRepository;
        $this->complementRepository = $complementRepository;
    }

    public function build(array $params = [])
    {
        $order = new Order();
        $order->setBurger($this->burgerFactory->build($params));

        if (!$user = $this->userRepository->byId($params['userId'])) {
            $user = new User();
        }

        $order->setUser($user);


        if ($params['complements']) foreach ($params['complements'] as $complementId) {
            if ($complement =$this->complementRepository->byId($complementId)) {
                    $order->addComplement($complement);
            }
        }

        return $order;
    }

}
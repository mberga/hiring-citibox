<?php

namespace App\Domain\BurrikingProduct\Service\Factory;


interface BurrikingFactory
{
    public function build(array $params = []);
}
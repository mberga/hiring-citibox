<?php

namespace App\Domain\BurrikingProduct\Service\Factory;


use App\Domain\BurrikingProduct\Entity\Burger;
use App\Domain\BurrikingProduct\Repository\BurgerSizeRepository;
use App\Domain\BurrikingProduct\Repository\IngredientRepository;
use App\Domain\BurrikingProduct\Repository\MeatTypeRepository;

class BurgerFactory implements BurrikingFactory
{
    protected $meatTypeRepository;
    protected $burgerSizeRepository;
    protected $ingredientRepository;

    public function __construct(
        MeatTypeRepository $meatTypeRepository,
        BurgerSizeRepository $burgerSizeRepository,
        IngredientRepository $ingredientRepository
    ) {
        $this->meatTypeRepository = $meatTypeRepository;
        $this->burgerSizeRepository = $burgerSizeRepository;
        $this->ingredientRepository = $ingredientRepository;
    }

    public function build(array $params = [])
    {
        $burger = new Burger();
        $burger->setBurgerSize($this->burgerSizeRepository->byId($params['burgerSize']))
            ->setMeatType($this->meatTypeRepository->byId($params['meatType']))
            ->setCookedStyle($params['cookedStyle']);

        if ($params['ingredients']) foreach ($params['ingredients'] as $ingredient) {
            $burger->addIngredient($this->ingredientRepository->byId($ingredient));
        }

        return $burger;
    }

}
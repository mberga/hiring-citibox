<?php


namespace App\Domain\BurrikingProduct\Repository;


use App\Domain\BurrikingProduct\Entity\Ingredient;

interface IngredientRepository
{
    public function byId(string $id): ?Ingredient;

    public function oneBy(array $params): ?Ingredient;

    public function by(array $params): ?array;

    public function save(Ingredient $ingredient);

    public function update(Ingredient $ingredient);

    public function delete(Ingredient $ingredient);
}
<?php

namespace App\Domain\BurrikingProduct\Repository;

use App\Domain\BurrikingProduct\Entity\Order;

interface OrderRepository
{
    public function byId(string $id): ?Order;

    public function oneBy(array $params): ?Order;

    public function by(array $params): ?array;

    public function save(Order $Order);

    public function update(Order $Order);

    public function delete(Order $Order);
}
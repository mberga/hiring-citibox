<?php


namespace App\Domain\BurrikingProduct\Repository;


use App\Domain\BurrikingProduct\Entity\BurgerSize;

interface BurgerSizeRepository
{
    public function byId(string $id): ?BurgerSize;

    public function oneBy(array $params): ?BurgerSize;

    public function by(array $params): ?array;

    public function save(BurgerSize $BurgerSize);

    public function update(BurgerSize $BurgerSize);

    public function delete(BurgerSize $BurgerSize);
}
<?php


namespace App\Domain\BurrikingProduct\Repository;


use App\Domain\BurrikingProduct\Entity\MeatType;

interface MeatTypeRepository
{
    public function byId(string $id): ?MeatType;

    public function oneBy(array $params): ?MeatType;

    public function by(array $params): ?array;

    public function save(MeatType $meatType);

    public function update(MeatType $meatType);

    public function delete(MeatType $meatType);
}
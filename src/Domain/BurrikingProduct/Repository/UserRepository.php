<?php

namespace App\Domain\BurrikingProduct\Repository;

use App\Domain\BurrikingProduct\Entity\User;

interface UserRepository
{
    public function byId(string $id): ?User;

    public function oneBy(array $params): ?User;

    public function by(array $params): ?array;

    public function save(User $user);

    public function update(User $user);

    public function delete(User $user);
}
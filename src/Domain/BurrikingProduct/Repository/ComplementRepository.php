<?php


namespace App\Domain\BurrikingProduct\Repository;


use App\Domain\BurrikingProduct\Entity\Complement;

interface ComplementRepository
{
    public function byId(string $id): ?Complement;

    public function oneBy(array $params): ?Complement;

    public function by(array $params): ?array;

    public function save(Complement $complement);

    public function update(Complement $complement);

    public function delete(Complement $complement);
}
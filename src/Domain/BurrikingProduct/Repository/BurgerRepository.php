<?php


namespace App\Domain\BurrikingProduct\Repository;


use App\Domain\BurrikingProduct\Entity\Burger;

interface BurgerRepository
{
    public function byId(string $id): ?Burger;

    public function oneBy(array $params): ?Burger;

    public function by(array $params): ?array;

    public function save(Burger $burguer);

    public function update(Burger $burguer);

    public function delete(Burger $burguer);
}
<?php


namespace App\Domain\BurrikingProduct\ValueObject;


use JMS\Serializer\Annotation as Serializer;

class Amount
{
    /**
     * @var int
     * @Serializer\Type("int")
     */
    protected $amount;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    protected $currency;

    /**
     * @var int
     * @Serializer\Type("int")
     */
    protected $precision;

    public static function create($amount, $currency = "EUR", $precision = 2)
    {
        $instance = new self();
        return $instance->setAmount($amount)
            ->setCurrency($currency)
            ->setPrecision($precision);
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return Amount
     */
    public function setAmount(int $amount): Amount
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return Amount
     */
    public function setCurrency(string $currency): Amount
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrecision(): int
    {
        return $this->precision;
    }

    /**
     * @param int $precision
     * @return Amount
     */
    public function setPrecision(int $precision): Amount
    {
        $this->precision = $precision;
        return $this;
    }
}
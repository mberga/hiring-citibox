<?php


namespace App\Domain\BurrikingProduct\Entity;


use App\Domain\BurrikingProduct\ValueObject\Amount;
use App\Domain\BurrikingProduct\ValueObject\EntityId;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Complement
 * @package App\Domain\BurrikingProduct\Entity
 * @Serializer\VirtualProperty(
 *     "id",
 *     exp="object.getId()",
 *     options={@Serializer\SerializedName("id")}
 * )
 */
class Complement
{
    const TYPE_DRINK = "drink";
    const TYPE_CHIPS = "chips";
    /**
     * @var EntityId
     * @Serializer\Type("string")
     */
    protected $id;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    protected $name;

    /**
     * @var Amount
     * @Serializer\Type("App\Domain\BurrikingProduct\ValueObject\Amount")
     */
    protected $price;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    protected $type;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    protected $size;

    /**
     * @var \DateTime
     * @Serializer\Type("DateTime")
     */
    protected $createdAt;

    public function __construct($id = null)
    {
        $this->id = new EntityId($id);
        $this->createdAt = new \DateTime();
    }

    /**
     * @return EntityId
     */
    public function getId(): EntityId
    {
        return new EntityId($this->id);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Complement
     */
    public function setName(string $name): Complement
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Amount
     */
    public function getPrice(): Amount
    {
        return $this->price;
    }

    /**
     * @param Amount $price
     * @return Complement
     */
    public function setPrice(Amount $price): Complement
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Complement
     */
    public function setType(string $type): Complement
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getSize(): string
    {
        return $this->size;
    }

    /**
     * @param string $size
     * @return Complement
     */
    public function setSize(?string $size): Complement
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Complement
     */
    public function setCreatedAt(\DateTime $createdAt): Complement
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
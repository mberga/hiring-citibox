<?php


namespace App\Domain\BurrikingProduct\Entity;


use App\Domain\BurrikingProduct\ValueObject\Amount;
use App\Domain\BurrikingProduct\ValueObject\EntityId;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Ingredient
 * @package App\Domain\BurrikingProduct\Entity
 * @Serializer\VirtualProperty(
 *     "id",
 *     exp="object.getId()",
 *     options={@Serializer\SerializedName("id")}
 * )
 */
class Ingredient
{

    /**
     * @var EntityId
     * @Serializer\Type("string")
     */
    protected $id;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    protected $name;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    protected $subtypes;

    /**
     * @var Amount
     * @Serializer\Type("App\Domain\BurrikingProduct\ValueObject\Amount")
     */
    protected $price;

    public function __construct($id = null)
    {
        $this->id = new EntityId($id);
        $this->createdAt = new \DateTime();
    }

    /**
     * @return EntityId
     */
    public function getId(): EntityId
    {
        return new EntityId($this->id);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Ingredient
     */
    public function setName(string $name): Ingredient
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubtypes(): string
    {
        return $this->subtypes;
    }

    /**
     * @param string $subtypes
     * @return Ingredient
     */
    public function setSubtypes(string $subtypes): Ingredient
    {
        $this->subtypes = $subtypes;
        return $this;
    }

    /**
     * @return Amount
     */
    public function getPrice(): Amount
    {
        return $this->price;
    }

    /**
     * @param Amount $price
     * @return Ingredient
     */
    public function setPrice(Amount $price): Ingredient
    {
        $this->price = $price;
        return $this;
    }
}
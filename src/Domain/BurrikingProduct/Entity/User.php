<?php

namespace App\Domain\BurrikingProduct\Entity;


use App\Domain\BurrikingProduct\ValueObject\EntityId;
use JMS\Serializer\Annotation as Serializer;


/**
 * Class MeatType
 * @package App\Domain\BurrikingProduct\Entity
 * @Serializer\VirtualProperty(
 *     "id",
 *     exp="object.getId()",
 *     options={@Serializer\SerializedName("id")}
 * )
 */
class User
{
    /**
     * @var EntityId
     * @Serializer\Type("string")
     */
    protected $id;

    /**
     * @var Order[]
     */
    protected $order;

    public function __construct($id = null)
    {
        $this->id = new EntityId($id);
    }

    /**
     * @return EntityId
     */
    public function getId(): EntityId
    {
        return $this->id;
    }

    /**
     * @return Order[]
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order[] $order
     * @return User
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }
}
<?php

namespace App\Domain\BurrikingProduct\Entity;


use App\Domain\BurrikingProduct\ValueObject\Amount;
use App\Domain\BurrikingProduct\ValueObject\EntityId;
use JMS\Serializer\Annotation as Serializer;

class Order
{
    const STATUS_CREATED = 0;
    const STATUS_PAYED = 1;
    const STATUS_SERVED = 2;

    /**
     * @var EntityId
     * @Serializer\Type("string")
     */
    protected $id;

    /**
     * @var User
     * @Serializer\Type("App\Domain\BurrikingProduct\Entity\User")
     */
    protected $user;

    /**
     * @var Burger
     * @Serializer\Type("App\Domain\BurrikingProduct\Entity\Burger")
     */
    protected $burger;

    /**
     * @var Complement[]
     * @Serializer\Type("array<App\Domain\BurrikingProduct\Entity\Complement>")
     */
    protected $complements;

    /**
     * @var Amount
     * @Serializer\Type("App\Domain\BurrikingProduct\ValueObject\Amount")
     */
    protected $price;

    /**
     * @var int
     * @Serializer\Type("int")
     */
    protected $status;

    /**
     * @var  \DateTime
     * @Serializer\Type("Datetime")
     */
    protected $createdAt;

    public function __construct($id = null)
    {
        $this->id = new EntityId($id);
        $this->createdAt = new \DateTime();
    }

    /**
     * @param EntityId $id
     * @return Order
     */
    public function setId(EntityId $id): Order
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Order
     */
    public function setUser(User $user): Order
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Burger
     */
    public function getBurger(): Burger
    {
        return $this->burger;
    }

    /**
     * @param Burger $burger
     * @return Order
     */
    public function setBurger(Burger $burger): Order
    {
        $this->burger = $burger;
        return $this;
    }

    /**
     * @return Complement[]
     */
    public function getComplements()
    {
        return $this->complements;
    }

    /**
     * @param Complement[] $complements
     * @return Order
     */
    public function setComplements($complements): Order
    {
        $this->complements = $complements;
        return $this;
    }

    public function addComplement(Complement $complement)
    {
        $this->complements[] = $complement;
        return $this;
    }

    /**
     * @return Amount
     */
    public function getPrice(): Amount
    {
        return $this->price;
    }

    /**
     * @param Amount $price
     * @return Order
     */
    public function setPrice(Amount $price): Order
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Order
     */
    public function setStatus(int $status): Order
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Order
     */
    public function setCreatedAt(\DateTime $createdAt): Order
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
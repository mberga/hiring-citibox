<?php


namespace App\Domain\BurrikingProduct\Entity;


use App\Domain\BurrikingProduct\ValueObject\Amount;
use App\Domain\BurrikingProduct\ValueObject\EntityId;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class BurgerSize
 * @package App\Domain\BurrikingProduct\Entity
 * @Serializer\VirtualProperty(
 *     "id",
 *     exp="Object.getId()",
 *     options={@Serializer\SerializedName("id")}
 * )
 * @Serializer\ExclusionPolicy("none")
 */
class BurgerSize
{
    /**
     * @var EntityId
     * @Serializer\Type("string")
     */
    protected $id;

    /**
     * @var int
     * @Serializer\Type("int")
     */
    protected $weight;

    /**
     * @var Amount
     * @Serializer\Type("App\Domain\BurrikingProduct\ValueObject\Amount")
     */
    protected $price;

    /**
     * @var  \DateTime
     * @Serializer\Type("DateTime")
     */
    protected $createdAt;

    public function __construct($id = null)
    {
        $this->id = new EntityId($id);
        $this->createdAt = new \DateTime();
    }

    /**
     * @return EntityId
     */
    public function getId(): EntityId
    {
        return new EntityId($this->id);
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     * @return BurgerSize
     */
    public function setWeight(int $weight): BurgerSize
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return Amount
     */
    public function getPrice(): Amount
    {
        return $this->price;
    }

    /**
     * @param Amount $price
     * @return BurgerSize
     */
    public function setPrice(Amount $price): BurgerSize
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return BurgerSize
     */
    public function setCreatedAt(\DateTime $createdAt): BurgerSize
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
<?php


namespace App\Domain\BurrikingProduct\Entity;


use App\Domain\BurrikingProduct\ValueObject\Amount;
use App\Domain\BurrikingProduct\ValueObject\EntityId;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class MeatType
 * @package App\Domain\BurrikingProduct\Entity
 * @Serializer\VirtualProperty(
 *     "id",
 *     exp="object.getId()",
 *     options={@Serializer\SerializedName("id")}
 * )
 */
class MeatType
{
    /**
     * @var EntityId
     * @Serializer\Type("string")
     */
    protected $id;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    protected $name;

    /**
     * @var Amount
     * @Serializer\Type("App\Domain\BurrikingProduct\ValueObject\Amount")
     */
    protected $price;

    /**
     * @var  \DateTime
     * @Serializer\Type("Datetime")
     */
    protected $createdAt;

    public function __construct($id = null)
    {
        $this->id = new EntityId($id);
        $this->createdAt = new \DateTime();
    }

    /**
     * @return EntityId
     */
    public function getId(): EntityId
    {
        return new EntityId($this->id);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return MeatType
     */
    public function setName(string $name): MeatType
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Amount
     */
    public function getPrice(): Amount
    {
        return $this->price;
    }

    /**
     * @param Amount $price
     * @return MeatType
     */
    public function setPrice(Amount $price): MeatType
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return MeatType
     */
    public function setCreatedAt(\DateTime $createdAt): MeatType
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
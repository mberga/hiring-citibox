<?php


namespace App\Domain\BurrikingProduct\Entity;


use App\Domain\BurrikingProduct\ValueObject\EntityId;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Burger
 * @package App\Domain\BurrikingProduct\Entity
 * @Serializer\VirtualProperty(
 *     "id",
 *     exp="object.getId()",
 *     options={@Serializer\SerializedName("id")}
 * )
 */
class Burger
{
    const COOKED_RARE = 0;
    const COOKED_MIDDLE = 1;
    const COOKED_WELL = 2;

    const BASE_PRICE = 500;

    /**
     * @var EntityId
     * @Serializer\Type("string")
     */
    protected $id;

    /**
     * @var BurgerSize
     * @Serializer\Type("App\Domain\BurrikingProduct\Entity\BurgerSize")
     */
    protected $burgerSize;

    /**
     * @var MeatType
     * @Serializer\Type("App\Domain\BurrikingProduct\Entity\MeatType")
     */
    protected $meatType;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    protected $cookedStyle;

    /**
     * @var Ingredient[]
     * @Serializer\Type("array<App\Domain\BurrikingProduct\Entity\Ingredient>")
     * @Serializer\Exclude()
     */
    protected $ingredients;

    /**
     * @var \DateTime
     * @Serializer\Type("DateTime")
     */
    protected $createdAt;

    public function __construct($id = null)
    {
        $this->id = new EntityId($id);
        $this->createdAt = new \DateTime();
    }

    /**
     * @return EntityId
     */
    public function getId(): EntityId
    {
        return new EntityId($this->id);
    }

    /**
     * @return BurgerSize
     */
    public function getBurgerSize(): BurgerSize
    {
        return $this->burgerSize;
    }

    /**
     * @param BurgerSize $burgerSize
     * @return Burger
     */
    public function setBurgerSize(BurgerSize $burgerSize): Burger
    {
        $this->burgerSize = $burgerSize;
        return $this;
    }

    /**
     * @return MeatType
     */
    public function getMeatType(): MeatType
    {
        return $this->meatType;
    }

    /**
     * @param MeatType $meatType
     * @return Burger
     */
    public function setMeatType(MeatType $meatType): Burger
    {
        $this->meatType = $meatType;
        return $this;
    }

    /**
     * @return string
     */
    public function getCookedStyle(): string
    {
        return $this->cookedStyle;
    }

    /**
     * @param string $cookedStyle
     * @return Burger
     */
    public function setCookedStyle(string $cookedStyle): Burger
    {
        $this->cookedStyle = $cookedStyle;
        return $this;
    }

    /**
     * @return Ingredient[]
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * @param array $ingredients
     * @return Burger
     */
    public function setIngredients($ingredients)
    {
        $this->ingredients = $ingredients;
        return $this;
    }

    public function addIngredient(Ingredient $ingredient)
    {
        $this->ingredients[] = $ingredient;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Burger
     */
    public function setCreatedAt(\DateTime $createdAt): Burger
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
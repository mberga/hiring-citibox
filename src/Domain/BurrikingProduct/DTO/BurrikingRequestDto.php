<?php

namespace App\Domain\BurrikingProduct\DTO;


interface BurrikingRequestDto
{
    public function to_array();
}
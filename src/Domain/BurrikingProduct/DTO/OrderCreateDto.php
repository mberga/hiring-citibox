<?php

namespace App\Domain\BurrikingProduct\DTO;


use App\Domain\BurrikingProduct\Entity\Ingredient;
use JMS\Serializer\Annotation as Serializer;

class OrderCreateDto implements  BurrikingRequestDto
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    protected $userId;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    protected $burgerSize;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    protected $meatType;

    /**
     * @var string[]
     * @Serializer\Type("array<string>")
     */
    protected $ingredients;

    /**
     * @var int
     * @Serializer\Type("int")
     */
    protected $cookedStyle;

    /**
     * @var string[]
     * @Serializer\Type("array<string>")
     */
    protected $complements;

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getBurgerSize(): ?string
    {
        return $this->burgerSize;
    }

    /**
     * @return string
     */
    public function getMeatType(): ?string
    {
        return $this->meatType;
    }

    /**
     * @return string[]
     */
    public function getIngredients(): ?array
    {
        return $this->ingredients;
    }

    /**
     * @return int
     */
    public function getCookedStyle(): ?int
    {
        return $this->cookedStyle;
    }

    /**
     * @return string[]
     */
    public function getComplements(): ?array
    {
        return $this->complements;
    }

    public function to_array()
    {
        return [
            "userId" => $this->getUserId(),
            "burgerSize" => $this->getBurgerSize(),
            "meatType" => $this->getMeatType(),
            "ingredients" => $this->getIngredients(),
            "cookedStyle" => $this->getCookedStyle(),
            "complements" => $this->getComplements()
        ];
    }
}
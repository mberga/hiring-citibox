<?php


namespace App\Domain\BurrikingProduct\Exception;


class InvalidUuidException extends \Exception
{
    public function __construct($message = "invalid uuid", $status = 500)
    {
        return parent::__construct($message, $status);
    }
}
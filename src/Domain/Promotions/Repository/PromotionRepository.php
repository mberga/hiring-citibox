<?php


namespace App\Domain\Promotions\Repository;

use App\Domain\Promotions\Entity\Promotion\Promotion;

interface PromotionRepository
{
    public function byId(string $id): ?Promotion;

    public function oneBy(array $params): ?Promotion;

    public function by(array $params): ?array;

    public function save(Promotion $promotion);

    public function update(Promotion $promotion);

    public function delete(Promotion $promotion);
}
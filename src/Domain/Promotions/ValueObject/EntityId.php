<?php


namespace App\Domain\Promotions\ValueObject;


use App\Domain\BurrikingProduct\Exception\InvalidUuidException;
use Ramsey\Uuid\Uuid;

class EntityId
{
    protected $id;

    /**
     * EntityId constructor.
     * Sets the current uuid if available, if not, creates a new random uuid
     * @param string|null $id
     * @throws InvalidUuidException
     */
    public function __construct(?string $id = null)
    {
        try {
            $this->id = Uuid::fromString($id ?: Uuid::uuid4()->toString());
        } catch (\InvalidArgumentException $e) {
            throw new InvalidUuidException();
        }
    }

    public function __toString(): string
    {
        return $this->id;
    }
}
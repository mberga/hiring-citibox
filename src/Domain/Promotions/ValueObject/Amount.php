<?php


namespace App\Domain\Promotions\ValueObject;


class Amount
{
    /** @var int */
    protected $amount;

    /** @var string */
    protected $currency;

    /** @var int */
    protected $precision;

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return Amount
     */
    public function setAmount(int $amount): Amount
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return Amount
     */
    public function setCurrency(string $currency): Amount
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrecision(): int
    {
        return $this->precision;
    }

    /**
     * @param int $precision
     * @return Amount
     */
    public function setPrecision(int $precision): Amount
    {
        $this->precision = $precision;
        return $this;
    }
}
<?php


namespace App\Domain\Promotions\Entity;


use App\Domain\Promotions\ValueObject\EntityId;

class Promotion
{
    /** @var EntityId */
    protected $id;

    /** @var string */
    protected $name;

    /** @var \DateTime */
    protected $startDate;

    /** @var \DateTime */
    protected $endDate;

    /** @var int */
    protected $weekDay;

    /** @var string */
    protected $startTime;

    /** @var string */
    protected $endTime;

    public function __construct($id = null)
    {
        $this->id = new EntityId($id);
        $this->createdAt = new \DateTime();
    }

    /**
     * @return EntityId
     */
    public function getId(): EntityId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Promotion
     */
    public function setName(string $name): Promotion
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     * @return Promotion
     */
    public function setStartDate(\DateTime $startDate): Promotion
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     * @return Promotion
     */
    public function setEndDate(\DateTime $endDate): Promotion
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeekDay(): int
    {
        return $this->weekDay;
    }

    /**
     * @param int $weekDay
     * @return Promotion
     */
    public function setWeekDay(int $weekDay): Promotion
    {
        $this->weekDay = $weekDay;
        return $this;
    }

    /**
     * @return string
     */
    public function getStartTime(): string
    {
        return $this->startTime;
    }

    /**
     * @param string $startTime
     * @return Promotion
     */
    public function setStartTime(string $startTime): Promotion
    {
        $this->startTime = $startTime;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndTime(): string
    {
        return $this->endTime;
    }

    /**
     * @param string $endTime
     * @return Promotion
     */
    public function setEndTime(string $endTime): Promotion
    {
        $this->endTime = $endTime;
        return $this;
    }
}
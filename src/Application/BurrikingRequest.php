<?php


namespace App\Application;


interface BurrikingRequest
{
    /**
     * Gets the content of the current request
     * @return string
     */
    public function getBody(): string;

    /**
     * Gets single paramater or default if not exists
     * @param string $param
     * @param string|null $default
     * @return string|null
     */
    public function getAttribute(string $param, string $default = null): ?string;

    /**
     * return all paramaters as array of $name => $value
     * @return array
     */
    public function getAttributes(): array;
}
<?php


namespace App\Application\UseCase;


use App\Application\BurrikingRequest;
use App\Domain\BurrikingProduct\Repository\BurgerSizeRepository;
use App\Domain\BurrikingProduct\Repository\ComplementRepository;
use App\Domain\BurrikingProduct\Repository\IngredientRepository;
use App\Domain\BurrikingProduct\Repository\MeatTypeRepository;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;

class ListProducts implements UseCase
{
    protected $burgerSizeRepository;
    protected $meatTypeRepository;
    protected $complementRepository;
    protected $ingredientRepository;
    protected $serializer;
    
    public function __construct(
        BurgerSizeRepository $burgerSizeRepository,
        MeatTypeRepository $meatTypeRepository,
        ComplementRepository $complementRepository,
        IngredientRepository $ingredientRepository,
        Serializer $serializer
    )
    {
        $this->burgerSizeRepository = $burgerSizeRepository;
        $this->meatTypeRepository = $meatTypeRepository;
        $this->complementRepository = $complementRepository;
        $this->ingredientRepository = $ingredientRepository;
        $this->serializer = $serializer;
    }

    public function execute(BurrikingRequest $request)
    {
        $sizes = $this->burgerSizeRepository->by([]);
        $meats = $this->meatTypeRepository->by([]);
        $complements = $this->complementRepository->by([]);
        $ingredients = $this->ingredientRepository->by([]);

        return $this->serializer->serialize(["burger" => ["sizes" => $sizes, "meats" => $meats, "ingredients" => $ingredients], "complements"=> $complements], 'json');
    }
}
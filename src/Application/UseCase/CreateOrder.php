<?php

namespace App\Application\UseCase;


use App\Application\BurrikingRequest;
use App\Domain\BurrikingProduct\DTO\OrderCreateDto;
use App\Domain\BurrikingProduct\Entity\Complement;
use App\Domain\BurrikingProduct\Entity\Order;
use App\Domain\BurrikingProduct\Repository\OrderRepository;
use App\Domain\BurrikingProduct\Service\Factory\BurrikingFactory;
use App\Domain\BurrikingProduct\Service\Pricing\PricingCalculator;
use App\Domain\BurrikingProduct\ValueObject\Amount;
use JMS\Serializer\Serializer;

class CreateOrder implements UseCase
{
    protected $serializer;
    protected $orderFactory;
    protected $burgerPriceCalculator;
    protected $orderRepository;

    public function __construct(
        Serializer $serializer,
        BurrikingFactory $orderFactory,
        PricingCalculator $burgerPriceCalculator,
        OrderRepository $orderRepository
    ) {
        $this->serializer = $serializer;
        $this->orderFactory = $orderFactory;
        $this->burgerPriceCalculator = $burgerPriceCalculator;
        $this->orderRepository = $orderRepository;
    }

    public function execute(BurrikingRequest $request)
    {
        /** @var OrderCreateDto $orderDto */
        $orderDto = $this->serializer->deserialize(
            $request->getBody(),
            OrderCreateDto::class,
            'json'
        );

        /** @var Order $order */
        $order = $this->orderFactory->build($orderDto->to_array());
        $order->setStatus(Order::STATUS_CREATED)
            ->setPrice($this->calculatePrice($order));

        $this->orderRepository->save($order);

        dump($order);die;
        return $this->serializer->serialize($order, 'json');
    }

    /**
     * @param Order $order
     * @return Amount
     */
    protected function calculatePrice(Order $order)
    {
        $price = 0;
        $chipsCount = 0;
        $drinkCount = 0;
        $isMenu = false;

        $burgerPrice = $this->burgerPriceCalculator->calculate($order->getBurger())->getAmount();

        foreach ($order->getComplements() as $complement) {
            $varName = $complement->getType() . "Count";
            $$varName++;
            $price += $complement->getPrice()->getAmount();
        }

        if ($chipsCount > 0 && $drinkCount > 0) {
            $isMenu = true;
            $burgerPrice = $burgerPrice * 0.85;
        }

        $price += $burgerPrice;

        return Amount::create($price);
    }

}
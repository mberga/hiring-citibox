<?php


namespace App\Application\UseCase;


use App\Application\BurrikingRequest;

interface UseCase
{
    public function execute(BurrikingRequest $request);
}
<?php

namespace App\DataFixtures;

use App\Domain\BurrikingProduct\Entity\BurgerSize;
use App\Domain\BurrikingProduct\Entity\Complement;
use App\Domain\BurrikingProduct\Entity\Ingredient;
use App\Domain\BurrikingProduct\Entity\MeatType;
use App\Domain\BurrikingProduct\ValueObject\Amount;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        //create burguers!
        $burgerTypes = ['wagyu','pollo', 'cerdo', 'pescado', 'veggie', 'cebra', 'angus'];
        $burgerSizes = [
            ["size" => 125, "price" => 200],
            ["size" => 14, "price" => 250],
            ["size" => 380, "price" => 350]
        ];
        $ingredients = [
            ["type" => "tomate", "price" => 100, "subtypes" => ["cherry", "normal"]],
            ["type" => "queso", "price" => 150, "subtypes" => []]];
        $complements = [
            ["type" => "drink", "name" => "Burricola", "price" => 230, "size" => null],
            ["type" => "drink", "name" => "Burribeer", "price" => 250, "size" => null],
            ["type" => "drink", "name" => "Brawndo", "price" => 750, "size" => null],
            ["type" => "chips", "name" => "Patatas deluxe", "price" => 250, "size" => "small"],
            ["type" => "chips", "name" => "Patatas deluxe", "price" => 350, "size" => "big"],
            ["type" => "chips", "name" => "Patatas de gajo", "price" => 250, "size" => "small"],
            ["type" => "chips", "name" => "Patatas de gajo", "price" => 350, "size" => "big"],
            ["type" => "chips", "name" => "Patatas de la abuela", "price" => 350, "size" => "small"],
            ["type" => "chips", "name" => "Patatas de la abuela", "price" => 350, "size" => "big"],
        ];

        foreach ($burgerTypes as $burgerType) {
            $entity = (new MeatType())
                ->setName($burgerType)
                ->setPrice(Amount::create(0));
            $manager->persist($entity);
        }

        foreach ($burgerSizes as $burgerSize) {
            $entity = (new BurgerSize())
                ->setWeight($burgerSize['size'])
                ->setPrice(Amount::create($burgerSize['price']));
            $manager->persist($entity);
        }

        foreach ($ingredients as $ingredient) {
            $entity = (new Ingredient())
                ->setName($ingredient['type'])
                ->setSubtypes(json_encode($ingredient['subtypes']))
                ->setPrice(Amount::create($ingredient['price']));
            $manager->persist($entity);
        }

        foreach ($complements as $complement) {
            $entity = (new Complement())
                ->setName($complement['name'])
                ->setType($complement['type'])
                ->setSize($complement['size'])
                ->setPrice(Amount::create($complement['price']));
            $manager->persist($entity);
        }

        $manager->flush();
    }
}

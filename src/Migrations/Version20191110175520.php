<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191110175520 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE burger_ingredient (burger_id VARCHAR(38) NOT NULL, ingredient_id VARCHAR(38) NOT NULL, INDEX IDX_340D596D17CE5090 (burger_id), INDEX IDX_340D596D933FE08C (ingredient_id), PRIMARY KEY(burger_id, ingredient_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE burger_ingredient ADD CONSTRAINT FK_340D596D17CE5090 FOREIGN KEY (burger_id) REFERENCES burguer (id)');
        $this->addSql('ALTER TABLE burger_ingredient ADD CONSTRAINT FK_340D596D933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE burger_ingredient');
    }
}

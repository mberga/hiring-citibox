<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191106135847 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE burguer (id VARCHAR(38) NOT NULL, burger_size_id VARCHAR(38) DEFAULT NULL, meat_type__id VARCHAR(38) DEFAULT NULL, cooked_style VARCHAR(40) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_BF2479C1B31D350 (burger_size_id), INDEX IDX_BF2479C343DF9BF (meat_type__id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE burguer_size (id VARCHAR(38) NOT NULL, weight INT NOT NULL, created_at DATETIME NOT NULL, price_amount INT DEFAULT NULL, price_currency VARCHAR(255) DEFAULT NULL, price_precision INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE meat_type (id VARCHAR(38) NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, price_amount INT DEFAULT NULL, price_currency VARCHAR(255) DEFAULT NULL, price_precision INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE burguer ADD CONSTRAINT FK_BF2479C1B31D350 FOREIGN KEY (burger_size_id) REFERENCES burguer_size (id)');
        $this->addSql('ALTER TABLE burguer ADD CONSTRAINT FK_BF2479C343DF9BF FOREIGN KEY (meat_type__id) REFERENCES meat_type (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE burguer DROP FOREIGN KEY FK_BF2479C1B31D350');
        $this->addSql('ALTER TABLE burguer DROP FOREIGN KEY FK_BF2479C343DF9BF');
        $this->addSql('DROP TABLE burguer');
        $this->addSql('DROP TABLE burguer_size');
        $this->addSql('DROP TABLE meat_type');
    }
}

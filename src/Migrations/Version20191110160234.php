<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191110160234 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id VARCHAR(38) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `order` (id VARCHAR(38) NOT NULL, burger_id VARCHAR(38) DEFAULT NULL, status INT NOT NULL, created_at DATETIME NOT NULL, price_amount INT DEFAULT NULL, price_currency VARCHAR(255) DEFAULT NULL, price_precision INT DEFAULT NULL, UNIQUE INDEX UNIQ_F529939817CE5090 (burger_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_complement (order_id VARCHAR(38) NOT NULL, complement_id VARCHAR(38) NOT NULL, INDEX IDX_4B7EE3618D9F6D38 (order_id), INDEX IDX_4B7EE36140D9D0AA (complement_id), PRIMARY KEY(order_id, complement_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_order (order_id VARCHAR(38) NOT NULL, user_id VARCHAR(38) NOT NULL, INDEX IDX_17EB68C08D9F6D38 (order_id), INDEX IDX_17EB68C0A76ED395 (user_id), PRIMARY KEY(order_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F529939817CE5090 FOREIGN KEY (burger_id) REFERENCES burguer (id)');
        $this->addSql('ALTER TABLE order_complement ADD CONSTRAINT FK_4B7EE3618D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE order_complement ADD CONSTRAINT FK_4B7EE36140D9D0AA FOREIGN KEY (complement_id) REFERENCES complement (id)');
        $this->addSql('ALTER TABLE user_order ADD CONSTRAINT FK_17EB68C08D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE user_order ADD CONSTRAINT FK_17EB68C0A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_order DROP FOREIGN KEY FK_17EB68C0A76ED395');
        $this->addSql('ALTER TABLE order_complement DROP FOREIGN KEY FK_4B7EE3618D9F6D38');
        $this->addSql('ALTER TABLE user_order DROP FOREIGN KEY FK_17EB68C08D9F6D38');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE `order`');
        $this->addSql('DROP TABLE order_complement');
        $this->addSql('DROP TABLE user_order');
    }
}

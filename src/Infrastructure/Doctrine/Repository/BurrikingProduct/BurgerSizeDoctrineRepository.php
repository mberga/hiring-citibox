<?php


namespace App\Infrastructure\Doctrine\Repository\BurrikingProduct;


use App\Domain\BurrikingProduct\Entity\BurgerSize;
use App\Domain\BurrikingProduct\Repository\BurgerSizeRepository;
use Doctrine\ORM\EntityRepository;

class BurgerSizeDoctrineRepository extends EntityRepository implements BurgerSizeRepository
{
    public function byId(string $id): ?BurgerSize
    {
        return $this->find($id);
    }

    public function oneBy(array $params): ?BurgerSize
    {
        return $this->findOneBy($params);
    }

    public function by(array $params): ?array
    {
        return $this->findBy($params);
    }

    public function save(BurgerSize $burgerSize)
    {
        $this->_em->persist($burgerSize);
        $this->_em->flush();
    }

    public function update(BurgerSize $burgerSize)
    {
        $this->save($burgerSize);
    }

    public function delete(BurgerSize $burgerSize)
    {
        $this->_em->remove($burgerSize);
        $this->_em->flush();
    }
}
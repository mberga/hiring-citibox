<?php

namespace App\Infrastructure\Doctrine\Repository\BurrikingProduct;


use App\Domain\BurrikingProduct\Entity\User;
use App\Domain\BurrikingProduct\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;

class UserDoctrineRepository extends EntityRepository implements UserRepository
{
    public function byId(string $id): ?User
    {
        return $this->find($id);
    }

    public function oneBy(array $params): ?User
    {
        return $this->findOneBy($params);
    }

    public function by(array $params): ?array
    {
        return $this->findBy($params);
    }

    public function save(User $User)
    {
        $this->_em->persist($User);
        $this->_em->flush();
    }

    public function update(User $user)
    {
        $this->save($user);
    }

    public function delete(User $user)
    {
        $this->_em->remove($user);
        $this->_em->flush();
    }

}
<?php


namespace App\Infrastructure\Doctrine\Repository\BurrikingProduct;


use App\Domain\BurrikingProduct\Entity\Complement;
use App\Domain\BurrikingProduct\Repository\ComplementRepository;
use Doctrine\ORM\EntityRepository;

class ComplementDoctrineRepository extends EntityRepository implements ComplementRepository
{
    public function byId(string $id): ?Complement
    {
        return $this->find($id);
    }

    public function oneBy(array $params): ?Complement
    {
        return $this->findOneBy($params);
    }

    public function by(array $params): ?array
    {
        return $this->findBy($params);
    }

    public function save(Complement $complement)
    {
        $this->_em->persist($complement);
        $this->_em->flush();
    }

    public function update(Complement $complement)
    {
        $this->save($complement);
    }

    public function delete(Complement $complement)
    {
        $this->_em->remove($complement);
        $this->_em->flush();
    }
}
<?php


namespace App\Infrastructure\Doctrine\Repository\BurrikingProduct;


use App\Domain\BurrikingProduct\Entity\Burger;
use App\Domain\BurrikingProduct\Repository\BurgerRepository;
use Doctrine\ORM\EntityRepository;

class BurgerDoctrineRepository extends EntityRepository implements BurgerRepository
{
    public function by(array $params): ?array
    {
        return $this->findBy($params);
    }

    public function byId(string $id): ?Burger
    {
        return $this->find($id);
    }

    public function oneBy(array $params): ?Burger
    {
        return $this->findOneBy($params);
    }

    public function update(Burger $burguer)
    {
        $this->save($burguer);
    }

    public function save(Burger $burguer)
    {
        $this->_em->persist($burguer);
        $this->_em->flush();
    }

    public function delete(Burger $burguer)
    {
        $this->_em->remove($burguer);
        $this->_em->flush();
    }
}

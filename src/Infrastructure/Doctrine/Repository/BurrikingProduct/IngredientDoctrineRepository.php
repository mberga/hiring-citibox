<?php


namespace App\Infrastructure\Doctrine\Repository\BurrikingProduct;


use App\Domain\BurrikingProduct\Entity\Ingredient;
use App\Domain\BurrikingProduct\Repository\IngredientRepository;
use Doctrine\ORM\EntityRepository;

class IngredientDoctrineRepository extends EntityRepository implements IngredientRepository
{
    public function byId(string $id): ?Ingredient
    {
        return $this->find($id);
    }

    public function oneBy(array $params): ?Ingredient
    {
        return $this->findOneBy($params);
    }

    public function by(array $params): ?array
    {
        return $this->findBy($params);
    }

    public function save(Ingredient $ingredient)
    {
        $this->_em->persist($ingredient);
        $this->_em->flush();
    }

    public function update(Ingredient $ingredient)
    {
        $this->save($ingredient);
    }

    public function delete(Ingredient $ingredient)
    {
        $this->_em->remove($ingredient);
        $this->_em->flush();
    }
}
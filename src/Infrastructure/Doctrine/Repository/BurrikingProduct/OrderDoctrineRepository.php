<?php

namespace App\Infrastructure\Doctrine\Repository\BurrikingProduct;


use App\Domain\BurrikingProduct\Repository\OrderRepository;
use Doctrine\ORM\EntityRepository;
use App\Domain\BurrikingProduct\Entity\Order;

class OrderDoctrineRepository extends EntityRepository implements OrderRepository
{
    public function byId(string $id): ?Order
    {
        return $this->find($id);
    }

    public function oneBy(array $params): ?Order
    {
        return $this->findOneBy($params);
    }

    public function by(array $params): ?array
    {
        return $this->findBy($params);
    }

    public function save(Order $order)
    {
        $this->_em->persist($order);
        $this->_em->flush();
    }

    public function update(Order $order)
    {
        $this->save($order);
    }

    public function delete(Order $order)
    {
        $this->_em->remove($order);
        $this->_em->flush();
    }
}
<?php


namespace App\Infrastructure\Doctrine\Repository\BurrikingProduct;


use App\Domain\BurrikingProduct\Entity\MeatType;
use App\Domain\BurrikingProduct\Repository\MeatTypeRepository;
use Doctrine\ORM\EntityRepository;

class MeatTypeDoctrineRepository extends EntityRepository implements MeatTypeRepository
{
    public function byId(string $id): ?MeatType
    {
        return $this->find($id);
    }

    public function oneBy(array $params): ?MeatType
    {
        return $this->findOneBy($params);
    }

    public function by(array $params): ?array
    {
        return $this->findBy($params);
    }

    public function save(MeatType $meatType)
    {
        $this->_em->persist($meatType);
        $this->_em->flush();
    }

    public function update(MeatType $meatType)
    {
        $this->save($meatType);
    }

    public function delete(MeatType $meatType)
    {
        $this->_em->remove($meatType);
        $this->_em->flush();
    }
}
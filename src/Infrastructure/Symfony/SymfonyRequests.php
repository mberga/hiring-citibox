<?php


namespace App\Infrastructure\Symfony;


use App\Application\BurrikingRequest;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class SymfonyRequests implements BurrikingRequest
{
    protected $attributes;
    protected $body;

    public static function fromSymfony(SymfonyRequest $request): BurrikingRequest
    {
        $instance = new self();
        $instance->body = $request->getContent();
        foreach ($request->attributes->all() as $key => $value) {
            if (!in_array($key, ['_route', '_controller', '_route_params'])) {
                $instance->attributes[$key] = $value;
            }
        }
        foreach ($request->query->all() as $key => $value) {
            if ($key !== $request->getPathInfo() && $key !== '_route') {
                $instance->attributes[$key] = $value;
            }
        }
        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function getBody(): string
    {
        return $this->body;
    }
    /**
     * @inheritDoc
     */
    public function getAttribute(string $param, string $default = null): ?string
    {
        return isset($this->attributes[$param]) ? $this->attributes[$param] : null;
    }

    /**
     * @inheritDoc
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }
}
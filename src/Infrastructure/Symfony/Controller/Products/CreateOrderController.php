<?php

namespace App\Infrastructure\Symfony\Controller\Products;


use App\Application\UseCase\CreateOrder;
use App\Infrastructure\Symfony\SymfonyRequests;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateOrderController
{
    protected $useCase;

    public function __construct(CreateOrder $useCase)
    {
        $this->useCase = $useCase;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/order", methods={"POST"})
     */
    public function createOrder(Request $request)
    {
        $result = $this->useCase->execute(SymfonyRequests::fromSymfony($request));

        return new Response($result);
    }
}
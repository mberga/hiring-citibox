<?php


namespace App\Infrastructure\Symfony\Controller\Products;


use App\Application\UseCase\ListProducts;
use App\Infrastructure\Symfony\SymfonyRequests;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductListController extends AbstractController
{
    protected $listProductsUSeCase;

    public function __construct(ListProducts $listProductsUseCase)
    {
        $this->listProductsUSeCase = $listProductsUseCase;
    }

    /**
     * @param Request $request
     * @return string
     * @Route("/products")
     */
    public function listProducts(Request $request)
    {
        return new Response($this->listProductsUSeCase->execute(SymfonyRequests::fromSymfony($request)));
    }
}
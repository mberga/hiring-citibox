<?php

namespace App\Infrastructure\Symfony\Controller;

use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends AbstractController
{
    protected $client;

    public function __construct(Client $commitClient)
    {
        $this->client = $commitClient;
    }
    public function index(Request $request)
    {

        $connection = $this->getDoctrine()->getConnection();
        $connection->connect();


        return new JsonResponse([
            "host" => $request->getHost(),
            "database" => $connection->isConnected() ? $connection->getHost() . " - OK" : "KO",
            "last_commit" => str_replace("\n", "", $this->client->get("/index.txt")->getBody()->getContents())
        ]);
    }
}